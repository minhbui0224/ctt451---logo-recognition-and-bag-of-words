Bài toán nhận diện logo có nhiều ứng dụng, ví dụ nhận diện nhãn hiệu xe trong nhà gửi xe, nhận diện logo quảng cáo, nhận diện các mặt hàng bị đặt sai chỗ trong siêu thị. Phương pháp nhận diện logo chuẩn, tức là logo nằm trên nền trắng hoặc phần nền không bị biến đổi nhiều, đạt được nhiều kết quả tốt. Nhưng trong thực tế, logo khó có thể nhận diện được do logo bị biến đổi về kích thước hình dạng và nằm trên những vân ảnh (texture) khác nhau. Bên cạnh việc logo cung cấp những thông tin tiên nghiệm như câu chữ bao hàm trong logo và hình dạng hình học của nó, mỗi lớp logo còn có nhiều hình thể của nó, gây khó khăn cho việc nhận dạng bằng cách so khớp với một ảnh mẫu.Vấn đề đặt ra trong đề tài này là: Cho trước tập dữ liệu các logo của các lớp logo khác nhau và một ảnh truy vấn có logo, ta cần tìm logo có trong hình thuộc lớp nào. Tập dữ liệu có thể chứa ít logo thuộc mối lớp, tuy nhiên có thể có nhiều lớp logo khác nhau. Việc có nhiều hơn một ảnh thuộc một lớp logo giúp cho việc phân lớp logo có nhiều hình thể có hiệu quả hơn.

TOAN BO PROJECT va DATA:

https://drive.google.com/drive/folders/0B6ffRMsXBp5EbFhCclBaa0NuanM?usp=sharing


*********************************************************************************
Moi truong
- Linux 
- Anaconda2-4.2.0-Linux-x86_64
- OpenCV 3
- seaborn  (CAI DAT THONG QUA CONDA)


Sau khi chay cac file tren thu muc ../data/experiment/bow{k}/ se gom:
codebook.npy : codebook
X.npy va y.npy : du lieu train mo hinh naive bayes
confusionMatrix.xlsx: ma tran confusion cua viec du doan.

Dataset:  flickr_logos_27_dataset
